﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositorio.Scripts
{
    public static class Usuario
    {
        public const string ObterUsuarioPorLogin = @"SELECT	u.Id,
                                                            u.IdPessoaFisica,
                                                            pf.Nome,
                                                            u.Login,
                                                            u.Senha,
                                                            u.DataInclusao,
                                                            u.Ativo 
                                                    FROM Usuario u
                                                    INNER JOIN PessoaFisica pf ON pf.id = u.IdPessoaFisica
                                                    WHERE Login like @Login";

        public const string ValidaLogin = @"SELECT	count(1)
                                            FROM Usuario WHERE Login = @Login
                                            and Senha = @Senha";
    }
}
