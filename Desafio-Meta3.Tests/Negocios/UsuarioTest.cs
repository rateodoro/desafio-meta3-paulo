﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Desafio_Meta3.Tests.Negocios
{
    [TestClass]
    public class UsuarioTest
    {
        [TestMethod]
        public void ObterUsuarioPorLogin()
        {
            var usuario = Negocio.ControleUsuario.ObterUsuarioPorLogin("joao");

            Assert.IsNotNull(usuario);
        }
        [TestMethod]
        public void ValidaLogin()
        {
            var valido = Negocio.ControleUsuario.ValidaLogin("joao", "123456");
            var resultadoEsperado = true;
            Assert.AreEqual(valido, resultadoEsperado);
        }
    }
}
